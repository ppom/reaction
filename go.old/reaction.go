package main

import (
	"framagit.org/ppom/reaction/app"
)

func main() {
	app.Main(version)
}

var (
	version = "v1.4.2"
)
